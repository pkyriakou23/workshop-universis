import { Component, OnInit } from '@angular/core';
import {UserService} from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {

    public profile: any = {
        person: {
            name: "Aliquamed Volutpatortor"
        },
        studyProgram: {
            name:"Vivamus quis elit eget odio porta egestas elit",
        },
        department: {
            name:"In hac habitasse platea dictumst"
        },
        studentStatus: {
            name: "Nullam tincidunt"
        },
        inscriptionPeriod: {
            name: "Tinci"
        },
        inscriptionYear: 2000
    };
    public loading: boolean = true;


  constructor(private _userService: UserService,
            private context: AngularDataContext,
            private _profileService: ProfileService) {
      //
  }

  ngOnInit() {
    this._profileService.getStudent().then((res) => {
        this.profile = res;
        this.loading = false;
    });
  }
}
