import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { MostModule} from '@themost/angular';
import { ProfileService } from '../../../profile/services/profile.service';
import { RouterTestingModule } from '@angular/router/testing';

import { RegistrationSpecialtyComponent } from './registration-specialty.component';
import {TestingConfigurationService} from '../../../test';

describe('RegistrationSpecialtyComponent', () => {
    // tslint:disable-next-line:prefer-const
  let component: RegistrationSpecialtyComponent;
    // tslint:disable-next-line:prefer-const
  let fixture: ComponentFixture<RegistrationSpecialtyComponent>;

  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  const currRegSvc = jasmine.createSpyObj('CurrentRegistrationService', ['getLastRegistration', 'getCurrentRegistrationEffectiveStatus',
            'getAvailableClasses', 'getStudyLevel', 'getSpecialties', 'getCurrentSpecialty', 'setSpecialty', 'getCurrentRegistration',
            'saveCurrentRegistration', 'registerForCourse', 'identifierCompare', 'removeCourse', 'getStudentStatus', 'reset',
            'registerSemester', 'getCurrentRegisterAction', 'getRegistrationStatus']);

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ RegistrationSpecialtyComponent ],
      imports: [ HttpClientTestingModule,
          RouterTestingModule,
          FormsModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          })],
      providers: [
        BsModalRef,
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          },
          {
              provide: ProfileService,
              useValue: profileSvc
          },
          {
              provide: CurrentRegistrationService,
              useValue: currRegSvc
          },
        ]
    })
    .compileComponents();
  }));

});
