import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {ProfileService} from '../profile/services/profile.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  providers: [ProfileService]
})
export class DashboardComponent {
  public student: any;
  /*
     if is true it will shown component progress-bar-semester
     if is false it will shown component progress-bar-degree
    */
  public viewsemester = false;

  constructor(private _profileService: ProfileService) {
    this._profileService.getStudent().then(res => {
      this.student = res; // Load data
    });
  }

}
